# useR! 2019

[www.user2019.fr/](https://www.user2019.fr/)

## Poster

* R packaging development using GitLab CI/CD : Build your own pipeline
  * [HAL](https://hal.archives-ouvertes.fr/hal-02284507)
  * [Poster](useR2019-R_packages_GitLab_JF_REY.pdf)


![Slide presentation](L93_JF_REY.jpg)

